;; (require 'exwm )
;; (require 'exwm-config)
;; (exwm-config-default)
;; (require 'exwm-systemtray)
;; (exwm-systemtray-enable)
;; (setq exwm-systemtray-height 16)
;; (require 'exwm-randr)
;; (setq exwm-workspace-number 2
;;  exwm-randr-workspace-output-plist '(0 "DisplayPort-3" 1 "HDMI-A-0"))
;; (add-hook 'exwm-randr-screen-change-hook
;;           (lambda ()
;;             (start-process-shell-command
;;              "xrandr" nil "xrandr --output DisplayPort-3 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-A-0 --mode 1920x1080 --pos 1920x0 --rotate normal")))
;; (exwm-randr-enable)

;; Computer Control functions
;; (defun dlt/screen-left (
;;     (interactive)
;;     (start-process-shell-command
;;      "xrandr" nil "xrandr --output DisplayPort-3 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-A-0 --off")
;;     )
;; (defun dlt/screen-both()
;;     (interactive)
;;     (start-process-shell-command
;;      "xrandr" nil "xrandr --output DisplayPort-3 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-A-0 --mode 1920x1080 --pos 1920x0 --rotate normal"))
;; (defun dlt/screen-left()
;;     (interactive)
;;     (start-process-shell-command
;;      "xrandr" nil "xrandr --output HDMI-A-0 --mode 1920x1080 --pos 0x0 --rotate normal --output DisplayPort-3 -off")
;;     )
;; (setq exwm-input-prefix-keys '(?\M-x
;;                                ?\M-:)
;;       exwm-input-global-keys '(([?\s-&] . (lambda (command)
;;                                              (interactive (list (read-shell-command "$ ")))
;;                                              (start-process-shell-command command nil command)))
;;                                ;; splits
;;                                ([?\s-v] . evil-window-vsplit)
;;                                ([?\s-z] . evil-window-split)
;;                                ;; managing workspaces
;;                                ([?\s-w] . exwm-workspace-switch)
;;                                ([?\s-W] . exwm-workspace-swap)
;;                                ([?\s-\C-w] . exwm-workspace-move)
;;                                ;; essential programs
;;                                ([?\s-d] . dired)
;;                                ([s-S-return] . dmenu)
;;                                ;; killing buffers and windows
;;                                ([?\s-b] . ibuffer)
;;                                ([?\s-B] . kill-current-buffer)
;;                                ([?\s-C] . +workspace/close-window-or-workspace)
;;                                ;; change window focus with super+h,j,k,l
;;                                ([?\s-h] . evil-window-left)
;;                                ([?\s-j] . evil-window-next)
;;                                ([?\s-k] . evil-window-prev)
;;                                ([?\s-l] . evil-window-right)
;;                                ;; move windows around using SUPER+SHIFT+h,j,k,l
;;                                ([?\s-H] . +evil/window-move-left)
;;                                ([?\s-J] . +evil/window-move-down)
;;                                ([?\s-K] . +evil/window-move-up)
;;                                ([?\s-L] . +evil/window-move-right)
;;                                ;; move window to far left or far right with SUPER+CTRL+h,l
;;                                ([?\s-\C-h] . side-left-window)
;;                                ([?\s-\C-j] . side-bottom-window)
;;                                ([?\s-\C-l] . side-right-window)
;;                                ([?\s-\C-d] . side-window-delete-all)
;;                                ([?\s-\C-r] . resize-window)
;;                                ;; switch workspace with SUPER+{0-9}
;;                                ([?\s-0] . (lambda () (interactive) (exwm-workspace-switch-create 0)))
;;                                ([?\s-1] . (lambda () (interactive) (exwm-workspace-switch-create 1)))
;;                                ([?\s-2] . (lambda () (interactive) (exwm-workspace-switch-create 2)))
;;                                ([?\s-3] . (lambda () (interactive) (exwm-workspace-switch-create 3)))
;;                                ([?\s-4] . (lambda () (interactive) (exwm-workspace-switch-create 4)))
;;                                ([?\s-5] . (lambda () (interactive) (exwm-workspace-switch-create 5)))
;;                                ;; move window workspace with SUPER+SHIFT+{0-9}
;;                                ([?\s-\)] . (lambda () (interactive) (exwm-workspace-move-window 0)))
;;                                ([?\s-!] . (lambda () (interactive) (exwm-workspace-move-window 1)))
;;                                ([?\s-@] . (lambda () (interactive) (exwm-workspace-move-window 2)))
;;                                ([?\s-#] . (lambda () (interactive) (exwm-workspace-move-window 3)))
;;                                ([?\s-$] . (lambda () (interactive) (exwm-workspace-move-window 4)))
;;                                ([?\s-%] . (lambda () (interactive) (exwm-workspace-move-window 5)))
;;                                ;; SUPER+/ switches to char-mode (needed to pass commands in XWindows sometimes)
;;                                ;; SUPER+? switches us back to line-mode
;;                                ([?\s-/] . exwm-input-toggle-keyboard)
;;                                ([?\s-?] . exwm-reset)
;;                                ;; setting some toggle commands
;;                                ([?\s-f] . exwm-floating-toggle-floating)
;;                                ([?\s-m] . exwm-layout-toggle-mode-line)
;;                                ([?\s-r] . exwm-restart)
;;                                ([f11] . exwm-layout-toggle-fullscreen)))
;; (defun dlt/exwm-start-lxsession ()
;;  (interactive)
;;  (start-process-shell-command "lxsession" nil "lxsession"))
;; (defun dlt/exwm-start-nmapplet ()
;;  (interactive)
;;  (start-process-shell-command "nm-applet" nil "nm-applet"))
;; (defun dlt/exwm-start-volumeicon ()
;;  (interactive)
;;  (start-process-shell-command "volumeicon" nil "volumeicon"))
;; (defun dlt/exwm-start-blueberrytray ()
;;  (interactive)
;;  (start-process-shell-command "blueberry-tray" nil "blueberry-tray"))
;; (defun dlt/exwm-start-picom ()
;;  (interactive)
;;  (start-process-shell-command "picom" nil "picom -c /home/jake/.config/herbstluftwm/picom.conf"))
;; (defun dlt/exwm-start-applets ()
;;   (interactive)
;;   (start-process-shell-command "killall" nil "killall blueberry-tray volumeicon nm-applet"))
;; (exwm-enable)
;; (after! exwm
;;   (dlt/exwm-start-lxsession)
;;   (dlt/exwm-start-nmapplet)
;;   (dlt/exwm-start-volumeicon)
;;   (dlt/exwm-start-blueberrytray)
;;   (dlt/exwm-start-picom)
;;   (setq display-time-day-and-date t
;;         display-time-formate "%a %b %d, %Y (%H:%M)"
;;         dmenu-prompt-string "RUN: "))

(setq user-full-name "Jacob Stannix"
      user-mail-address "jakestannix@gmail.com")

(setq doom-theme 'doom-vibrant)

(require 'dired-x)
(setq-default dired-omit-files-p t)
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
(setq user-mail-address "jakestannix@gmail.com"
      user-full-name  "Jacob Stannix"
      ;; I have my mbsyncrc in a different folder on my system, to keep it separate from the
      ;; mbsyncrc available publicly in my dotfiles. You MUST edit the following line.
      ;; Be sure that the following command is: "mbsync -c ~/.config/mu4e/mbsyncrc -a"
      mu4e-get-mail-command "mbsync -c ~/.config/mu4e/mbsyncrc -a"
      mu4e-update-interval  300
      ;; mu4e-compose-signature
      ;;  (concat
      ;;    "Derek Taylor\n"
      ;;    "http://www.youtube.com/DistroTube\n")
      message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t
      smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
      smtpmail-auth-credentials '(("smtp.gmail.com" 587 "jakestannix@gmial.com" nil))
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      mu4e-sent-folder "/Sent"
      mu4e-drafts-folder "/Drafts"
      mu4e-trash-folder "/Trash"
      mu4e-refile-folder "/All Mail"
      ;; mu4e-maildir-shortcuts
      ;; '(("/derek-distrotube/Inbox"    . ?i)
      ;;   ("/derek-distrotube/Sent"     . ?s)
      ;;   ("/derek-distrotube/All Mail" . ?a)
      ;;   ("/derek-distrotube/Trash"    . ?t))
      )

(after! org
  (setq org-directory "~/Documents/org")
  (setq org-agenda-files '("~/Documents/org/Agenda/"))
)

(setq display-line-numbers-type t)

(defun my-paragraph ()
  (interactive)
  (+evil/insert-newline-below 1)
  (evil-next-line)
  (insert "\\paragraph{}")
  (backward-char)
  (evil-insert-state))
(defun my-section ()
  (interactive)
  (+evil/insert-newline-below 1)
  (evil-next-line)
  (insert "\\section{}")
  (backward-char)
  (evil-insert-state))
(defun my-titleformat ()
  (interactive)
  (+evil/insert-newline-below 1)
  (evil-next-line)
  (insert "\\titleformat{}
{}
{}
{0em}
{}")
(evil-previous-line)
(evil-previous-line)
(evil-previous-line)
(evil-previous-line)
(evil-org-append-line)
)

(map! :leader
      (:prefix-map ("z" . "Major modes")
       :desc "Org mode" "o" #'org-mode
      :desc "Zen mode" "z" #'zen-mode
      :desc "Sh mode" "s" #'sh-mode
      :desc "ansi term" "a" #'ansi-term
      :desc "auto fill mode" "f" #'auto-fill-mode
      :desc "nroff-mode" "g" #'nroff-mode)
      )
(map! :leader
      (:prefix-map ("l" . "Latex Shortcuts")
       :desc "insert block" "b" #'latex-insert-block
       :desc "close block" "c" #'latex-close-block
       :desc "brace" "l" #'tex-insert-braces
       :desc "paragraph" "p" #'my-paragraph
       :desc "section" "s" #'my-section
       :desc "format title" "t" #'my-titleformat)
)

(add-hook 'dired-load-hook '(lambda () (require 'dired-x))) ;

(add-hook 'dired-mode-hook 'auto-revert-mode)

(add-to-list 'auto-mode-alist
             '("\\.mom\\'" . (lambda ()
               (nroff-mode)
               )))
