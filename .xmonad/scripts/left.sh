#!/bin/bash
leftDisplay=DP-2
rightDisplay=HDMI-1
xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
nitrogen --restore
