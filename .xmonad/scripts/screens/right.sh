#!/bin/bash
leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
nitrogen --restore
