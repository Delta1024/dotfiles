#!/bin/bash
leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
xrandr --output ${leftDisplay} --auto --primary --output ${rightDisplay} --auto --right-of ${leftDisplay}
nitrogen --restore
