# Defined in - @ line 1
function ll --wraps='/bin/ls -lA --group-directories-first --color=auto' --description 'alias ll=/bin/ls -lA --group-directories-first --color=auto'
  /bin/ls -lA --group-directories-first --color=auto $argv;
end
