# Defined in - @ line 1
function lsn --wraps='/bin/ls --group-directories-first --color=auto' --wraps='/bin/ls -l --group-directories-first --color=auto' --wraps='/bin/ls -lA --group-directories-first --color=auto' --description 'alias lsn=/bin/ls --group-directories-first --color=auto'
  /bin/ls --group-directories-first --color=auto $argv;
end
