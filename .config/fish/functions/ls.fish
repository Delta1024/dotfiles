# Defined in - @ line 1
function ls --wraps='/bin/ls --group-directories-first --color=auto -l' --description 'alias ls=/bin/ls --group-directories-first --color=auto -l'
  /bin/ls --group-directories-first --color=auto -l $argv;
end
