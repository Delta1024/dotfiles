# Defined in - @ line 1
function paclist-sync --wraps='sudo pacman -Rsu (comm -23 (pacman -Qq | sort | psub) (sort $HOME/.config/archconfig/yaylist.txt | psub))' --description 'alias paclist-sync=sudo pacman -Rsu (comm -23 (pacman -Qq | sort | psub) (sort $HOME/.config/archconfig/yaylist.txt | psub))'
  sudo pacman -Rsu (comm -23 (pacman -Qq | sort | psub) (sort $HOME/.config/archconfig/yaylist.txt | psub)) $argv;
end
