# Defined in /home/jake/.config/fish/functions/doom.fish @ line 2
function doom --wraps=.emacs.d/bin/doom --description 'alias doom=.emacs.d/bin/doom'
  .emacs.d/bin/doom  $argv;
end
