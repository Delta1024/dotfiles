#!/bin/env bash
hc () {
	herbstclient $@
}

LEFTBAR=$(hc list_monitors | cut -d" " -f2 | cut -d"+" -f1 | sed 2d)
RIGHTBAR=$(hc list_monitors | cut -d" " -f2 | cut -d"+" -f1 | sed 1d)

case "$LEFTBAR $RIGHTBAR" in
	"1920x1080 1920x1060")
		BARSIDE="left"
		BARINIT="both"
		;;
	"1920x1060 1920x1060")
		# echo "both"
		BARSIDE="both"
		BARINIT="both"
		;;
	"1920x1060 1920x1080")
		BARSIDE="right"
		BARINIT="both"
		;;
	"1920x1080 1920x1080")
		BARSIDE=" "
		BARINIT=" "
esac
$HOME/.config/herbstluftwm/bar_toggle.sh "$BARINIT" && betterlockscreen -l && $HOME/.config/herbstluftwm/bar_toggle.sh "$BARSIDE"
