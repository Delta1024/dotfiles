#!/bin/env bash
# left
if [[ $1 = 'left' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    herbstclient reload
fi
# both
if [[ $1 = 'both' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --primary --output ${rightDisplay} --auto --right-of ${leftDisplay}
    # nitrogen --restore
    sleep 6
    herbstclient reload
fi
# right
if [[ $1 = 'right' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
    # nitrogen --restore
    herbstclient reload
    sleep 3
    killall polybar
    for m in $(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p'); do
        MONITOR=$m polybar mainbar-herbstluftwm &
    done
fi
# For games
if [[ $1 = 'games' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    killall polybar
    herbstclient floating on
    herbstclient set_monitors 1920x1080+0+0

fi

if [[ $1 = 'videos' ]]; then
leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    killall polybar
    herbstclient set_monitors 1920x1080+0+0
fi

# left
if [[ $1 = 'left' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    herbstclient reload
fi
# both
if [[ $1 = 'both' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --primary --output ${rightDisplay} --auto --right-of ${leftDisplay}
    # nitrogen --restore
    sleep 6
    herbstclient reload
fi
# right
if [[ $1 = 'right' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --off --output ${rightDisplay} --auto
    # nitrogen --restore
    herbstclient reload
    sleep 3
    killall polybar
    for m in $(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p'); do
        MONITOR=$m polybar mainbar-herbstluftwm &
    done
fi
# For games
if [[ $1 = 'games' ]]; then
    leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    killall polybar
    herbstclient floating on
    herbstclient set_monitors 1920x1080+0+0

fi

if [[ $1 = 'videos' ]]; then
leftDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '1p')
    rightDisplay=$(xrandr | grep " connected" | cut -d" " -f1 | sed -n '2p')
    xrandr --output ${leftDisplay} --auto --output ${rightDisplay} --off
    # nitrogen --restore
    killall polybar
    herbstclient set_monitors 1920x1080+0+0
fi
