#!/bin/env bash
# kills any running polybar instance and adjusts herbstluftwm acordinly
if [[ $1 = 'both' ]]; then
	if [ $(pgrep polybar | wc -l) = 1 ] || [ $(pgrep polybar | wc -l) = 2 ]; then
		killall polybar
		herbstclient detect_monitors
    else
        ~/.config/herbstluftwm/panel.sh
herbstclient set_monitors 1920x1060+0+20 1920x1060+1920+20
	fi
fi

# kills the left bar and keeps/activates the right bar
if [[ $1 = 'left' ]]; then
	if [ $(pgrep polybar | wc -l) -ge 0 ]; then
		killall polybar
		herbstclient set_monitors 1920x1080+0+0 1920x1060+1920+20
		for m in $(xrandr --query | grep " connected" | cut -d" " -f1 | sed -n '2p'); do
		    MONITOR=$m polybar mainbar-herbstluftwm &
		done
	fi
fi

# kills the right bar and keeps/activates the left bar
if [[ $1 = 'right' ]]; then
	if [ $(pgrep polybar | wc -l) -ge 0 ]; then
		killall polybar
		herbstclient set_monitors 1920x1060+0+20 1920x1080+1920+0
		for m in $(xrandr --query | grep " connected" | cut -d" " -f1 | sed -n '1p'); do
		    MONITOR=$m polybar mainbar-herbstluftwm &
		done
	fi
fi

# resets the bar to its default layout
if [[ $1 = 'reset' ]]; then
	killall polybar
	~/.config/herbstluftwm/panel.sh
	herbstclient set_monitors 1920x1060+0+20 1920x1060+1920+20
fi

# for starting the bar when the right screen is the only one visible.
if [[ $1 = '_right' ]]; then
	killall polybar
	for m in $(xrandr --query | grep " connected" | cut -d" " -f1 | sed -n '2p'); do
		    MONITOR=$m polybar mainbar-herbstluftwm &
	done
	herbstclient set_monitors 1920x1060+0+20 1920x1060+1920+20
fi
