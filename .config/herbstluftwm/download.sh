#!/bin/env bash
options="video\nNoPlaylist video\nmusic"
args=$(echo -e ${options} | sort)
prompt=$(echo -e "${args[@]}" | dmenu -i -p 'File:')
spawn_with_rules() { (
	herbstclient rule once pid=$BASHPID maxage=10 "${RULES[@]}"
	exec "$@"
) &
}
RULES=( floating=on )
case "$prompt" in
    video)
        cd $HOME/Videos/
	    url=$(xclip -o)
        spawn_with_rules  alacritty -e youtube-dl $url
;;
    'NoPlaylist video')
        cd $HOME/Videos/
        url=$(xclip -o)
        spawn_with_rules  alacritty -e youtube-dl --no-playlist $url
        ;;
    music)
     cd $HOME/Music/
	 url=$(xclip -o)
	spawn_with_rules alacritty -e youtube-dl -x $url
;;
esac
