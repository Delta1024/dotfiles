#!/bin/env bash
hc() {
	herbstclient $@
}
check=$(hc get_attr theme.border_width)

if [ $check == 0 ]; then
	hc set_attr theme.border_width 4
else
	hc set_attr theme.border_width 0
fi
