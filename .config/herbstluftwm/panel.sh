#!/bin/env bash
killall polybar
for m in $(xrandr --query | grep " connected" | cut -d" " -f1 | sed -n '1p' ); do
       	MONITOR=$m polybar mainbar-herbstluftwm &
done
for m in $(xrandr --query | grep " connected" | cut -d" " -f1 | sed -n '2p' ); do
       	MONITOR=$m polybar mainbar-herbstluftwm-sidebar &
done
