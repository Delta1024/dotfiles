#!/bin/env bash
network=nm-applet
volume=volumeicon
bluetooth=blueberry-tray

killall $network $volume $bluetooth
$network &
$volume &
$bluetooth &
